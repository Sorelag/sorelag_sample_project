package com.sorelag.sample.base.models.forecast

import com.sorelag.sample.base.ui.viewModels.forecast.ForecastViewModel
import io.reactivex.Observable

interface ForecastModel {

    fun getForecast(): Observable<ForecastViewModel>
}