package com.sorelag.sample.base.data.formatters

interface BaseFormatter<From, To> {

     fun format(from: From): To?
}