package com.sorelag.sample.base.di

import com.sorelag.sample.base.application.network.NetworkApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module(includes = [NetworkModule::class])
class ApiModule {

    @Singleton
    @Provides
    internal fun provideApi(@Named(NetworkModule.SIMPLE) retrofitAdapter: Retrofit): NetworkApi {
        return retrofitAdapter.create(NetworkApi::class.java)
    }
}