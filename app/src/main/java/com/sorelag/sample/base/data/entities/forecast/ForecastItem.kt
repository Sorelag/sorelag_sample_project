package com.sorelag.sample.base.data.entities.forecast

/**
{
    "date": "2019-10-29",
    "day": {[ForecastParams]},
    "night": {[ForecastParams]}
}
 **/
data class ForecastItem(
    val date: String,
    val day: ForecastParams,
    val night: ForecastParams
)
