package com.sorelag.sample.base.data.formatters

import android.text.Spannable
import android.text.SpannableString

class SimpleTemplateFormatter(private var template: String) : BaseFormatter<Double, Spannable> {

    override fun format(from: Double): Spannable? {
        val formattedText = String.format(template, from.toInt())
        return SpannableString(formattedText)
    }
}