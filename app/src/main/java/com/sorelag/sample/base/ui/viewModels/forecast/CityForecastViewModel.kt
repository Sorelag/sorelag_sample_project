package com.sorelag.sample.base.ui.viewModels.forecast

class CityForecastViewModel {
    lateinit var city: String
    var temperatureDay: Double? = null
    var temperatureNight: Double? = null
    lateinit var nightWeatherType: String
    lateinit var dayWeatherType: String
}

