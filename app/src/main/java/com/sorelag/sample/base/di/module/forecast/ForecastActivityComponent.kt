package com.sorelag.sample.base.di.module.forecast

import com.sorelag.sample.base.di.scopes.ActivityScope
import com.sorelag.sample.base.ui.forecast.ForecastActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = arrayOf(ForecastActivityModule::class))
interface ForecastActivityComponent {

    fun inject(activity: ForecastActivity)

    @Subcomponent.Builder
    interface Builder {

        fun module(module: ForecastActivityModule): Builder

        fun build(): ForecastActivityComponent
    }
}