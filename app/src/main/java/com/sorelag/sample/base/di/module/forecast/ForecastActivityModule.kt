package com.sorelag.sample.base.di.module.forecast

import com.sorelag.sample.base.application.utils.NetworkConnectionWrapper
import com.sorelag.sample.base.data.repositories.forecast.ForecastRepository
import com.sorelag.sample.base.di.RxModule
import com.sorelag.sample.base.di.scopes.ActivityScope
import com.sorelag.sample.base.models.forecast.ForecastModel
import com.sorelag.sample.base.models.forecast.ForecastModelImpl
import com.sorelag.sample.base.ui.forecast.ForecastActivity
import com.sorelag.sample.base.ui.forecast.ForecastActivityContract
import com.sorelag.sample.base.ui.forecast.ForecastActivityPresenter
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named

@Module
class ForecastActivityModule(private val activity: ForecastActivity) {

    @ActivityScope
    @Provides
    internal fun provideActivity(): ForecastActivity {
        return activity
    }

    @ActivityScope
    @Provides
    internal fun provideModel(
        forecastRepository: ForecastRepository
    ): ForecastModel {
        return ForecastModelImpl(forecastRepository)
    }

    @ActivityScope
    @Provides
    internal fun providePresenter(
        forecastModel: ForecastModel,
        connectionWrapper: NetworkConnectionWrapper,
        @Named(RxModule.MAIN) observerScheduler: Scheduler
    ): ForecastActivityContract.Presenter {
        return ForecastActivityPresenter(
            forecastModel,
            connectionWrapper,
            observerScheduler
        )
    }
}
