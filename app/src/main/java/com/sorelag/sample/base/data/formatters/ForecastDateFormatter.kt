package com.sorelag.sample.base.data.formatters

import com.sorelag.sample.base.application.utils.DateTimeUtils

class ForecastDateFormatter : BaseFormatter<String, String> {

    override fun format(from: String): String? {
        return DateTimeUtils.formatDateToMonth(from)
    }
}