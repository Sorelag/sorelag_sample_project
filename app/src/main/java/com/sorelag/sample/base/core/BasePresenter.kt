package com.sorelag.sample.base.core

import java.lang.ref.Reference
import java.lang.ref.WeakReference

abstract class BasePresenter<V : BasePresentationContract.BaseView> :
    BasePresentationContract.BasePresenter<V> {

    private var baseView: Reference<V>? = null

    protected val view: V?
        get() = baseView!!.get()

    override fun attachView(view: V) {
        this.baseView = WeakReference(view)
    }

    override fun detachView() {
        this.baseView!!.clear()
    }
}
