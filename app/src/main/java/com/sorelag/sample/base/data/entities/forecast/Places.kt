package com.sorelag.sample.base.data.entities.forecast

/**
{
    "name": "Harku",
    "phenomenon": "Light shower",
    "tempmin": null,
    "tempmax": 2.0
}
 **/
data class Places(
    val name: String,
    val phenomenon: String,
    val tempmin: Double,
    val tempmax: Double
)