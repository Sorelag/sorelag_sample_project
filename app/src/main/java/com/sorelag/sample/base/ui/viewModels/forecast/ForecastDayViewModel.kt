package com.sorelag.sample.base.ui.viewModels.forecast

class ForecastDayViewModel {

    lateinit var date: String
    var temperatureDayMin: Double? = null
    var temperatureDayMax: Double? = null
    var temperatureNightMin: Double? = null
    var temperatureNightMax: Double? = null
    lateinit var nightWeatherType: String
    lateinit var dayWeatherType: String
    lateinit var dayText: String
    lateinit var nightText: String
    lateinit var cityData: List<CityForecastViewModel>
    lateinit var cityNames: List<String>
}