package com.sorelag.sample.base.di

import com.sorelag.sample.base.application.network.NetworkApi
import com.sorelag.sample.base.data.repositories.forecast.ForecastRepository
import com.sorelag.sample.base.data.repositories.forecast.ForecastRepositoryImpl
import com.sorelag.sample.base.di.scopes.FeatureScope
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named

@Module
object RepositoryModule {

    @FeatureScope
    @JvmStatic
    @Provides
    internal fun provideForecastRepository(
        api: NetworkApi,
        @Named(RxModule.NETWORK) networkScheduler: Scheduler
    ): ForecastRepository {
        return ForecastRepositoryImpl(api, networkScheduler)
    }
}