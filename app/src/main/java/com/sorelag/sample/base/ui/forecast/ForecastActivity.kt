package com.sorelag.sample.base.ui.forecast

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.sorelag.sample.R
import com.sorelag.sample.base.application.utils.UiHelper
import com.sorelag.sample.base.core.BaseActivity
import com.sorelag.sample.base.data.formatters.ForecastDateFormatter
import com.sorelag.sample.base.data.formatters.SimpleTemplateFormatter
import com.sorelag.sample.base.di.component.AppComponent
import com.sorelag.sample.base.di.module.forecast.ForecastActivityModule
import com.sorelag.sample.base.ui.viewModels.forecast.CityForecastViewModel
import com.sorelag.sample.base.ui.viewModels.forecast.ForecastDayViewModel
import kotlinx.android.synthetic.main.activity_forecast.*
import javax.inject.Inject

// TODO add RxClick debounce for next/previous day buttons
class ForecastActivity : BaseActivity(),
    ForecastActivityContract.View {

    override fun setupDependencies(appComponent: AppComponent) {
        appComponent.forecastActivityComponent()
            .module(ForecastActivityModule(this))
            .build()
            .inject(this)
    }

    @Inject
    internal lateinit var mPresenter: ForecastActivityContract.Presenter

    private var mIsAllLoaded: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forecast)
        mPresenter.attachView(this)
        initUi()
        mPresenter.getForecastData()
        mIsAllLoaded = true
    }

    private fun initUi() {
        svRefreshLayout.setOnRefreshListener { mPresenter.getForecastData() }
        btnNext.setOnClickListener {
            mPresenter.getNextDayData()
        }

        btnPrev.setOnClickListener {
            mPresenter.getPreviousDayData()
        }

        spSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                if (mIsAllLoaded)
                    mPresenter.getCityData(parent.getItemAtPosition(position).toString())
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    override fun showProgress() {
        pvProgress.show()
    }

    override fun showError() {
        Toast.makeText(this, "Something wrong", Toast.LENGTH_LONG).show()
    }

    override fun hideProgress() {
        pvProgress.hide()
        svRefreshLayout.isRefreshing = false
    }

    override fun showNoConnection() {
        Toast.makeText(this, "No internet connection", Toast.LENGTH_LONG).show()
    }

    override fun showData(dataModel: ForecastDayViewModel) {
        tvTemperatureDay.text =
            SimpleTemplateFormatter(getString(R.string.temperature_template)).format(dataModel.temperatureDayMax!!)
        tvTemperatureNight.text =
            SimpleTemplateFormatter(getString(R.string.temperature_template)).format(dataModel.temperatureNightMin!!)
        tvDate.text = ForecastDateFormatter().format(dataModel.date)
        tvDayInfo.text = dataModel.dayText
        tvNightInfo.text = dataModel.nightText

        ivDay.setImageResource(UiHelper.getWeatherResource(dataModel.dayWeatherType))
        ivNight.setImageResource(UiHelper.getWeatherResource(dataModel.nightWeatherType))
    }

    override fun togglePreviousBtnVisibility(visible: Boolean) {
        btnPrev.visibility = if (visible) View.VISIBLE else View.INVISIBLE
    }

    override fun toggleNextBtnVisibility(visible: Boolean) {
        btnNext.visibility = if (visible) View.VISIBLE else View.INVISIBLE
    }

    override fun showCityData(data: List<String>) {
        val adapter = ArrayAdapter<String>(
            this@ForecastActivity,
            android.R.layout.simple_spinner_item, data
        )

        adapter.setDropDownViewResource(R.layout.spinner_item)
        spSpinner.adapter = adapter
    }

    override fun showCityInfo(cityModel: CityForecastViewModel) {
        tvTemperatureDay.text =
            SimpleTemplateFormatter(getString(R.string.temperature_template)).format(cityModel.temperatureDay!!)
        tvTemperatureNight.text =
            SimpleTemplateFormatter(getString(R.string.temperature_template)).format(cityModel.temperatureNight!!)
        ivDay.setImageResource(UiHelper.getWeatherResource(cityModel.dayWeatherType))
        ivNight.setImageResource(UiHelper.getWeatherResource(cityModel.nightWeatherType))
    }
}
