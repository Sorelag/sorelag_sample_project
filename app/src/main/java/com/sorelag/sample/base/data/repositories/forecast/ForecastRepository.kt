package com.sorelag.sample.base.data.repositories.forecast

import com.sorelag.sample.base.data.entities.forecast.Forecast
import io.reactivex.Observable


interface ForecastRepository {

    fun getForecast(): Observable<Forecast>
}