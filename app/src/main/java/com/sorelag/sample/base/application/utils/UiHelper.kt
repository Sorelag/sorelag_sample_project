package com.sorelag.sample.base.application.utils

import com.sorelag.sample.R
import com.sorelag.sample.base.data.entities.forecast.WeatherType

//TODO  Find better solution
object UiHelper {

    @JvmStatic
    fun getWeatherResource(type: String): Int {
        return when (type) {
            WeatherType.VARIABLE_CLOUDS -> R.drawable.ic_clouds
            WeatherType.LIGHT_RAIN, WeatherType.MODERATE_RAIN -> R.drawable.ic_rain
            WeatherType.LIGHT_SLEET, WeatherType.MODERATE_SLEET -> R.drawable.ic_sleet
            WeatherType.LIGHT_SHOWER -> R.drawable.ic_shower
            WeatherType.LIGHT_SNOW_SHOWER -> R.drawable.ic_snow
            WeatherType.CLOUDY_CLEAR_SPELLS -> R.drawable.ic_cloud_spells
            else -> R.drawable.ic_cloud
        }
    }
}