package com.sorelag.sample.base.ui.forecast

import com.sorelag.sample.base.core.BasePresentationContract
import com.sorelag.sample.base.ui.viewModels.forecast.CityForecastViewModel
import com.sorelag.sample.base.ui.viewModels.forecast.ForecastDayViewModel

interface ForecastActivityContract {

    interface View : BasePresentationContract.BaseView {

        fun showProgress()

        fun hideProgress()

        fun showData(dataModel: ForecastDayViewModel)

        fun showError()

        fun showNoConnection()

        fun togglePreviousBtnVisibility(visible: Boolean)

        fun toggleNextBtnVisibility(visible: Boolean)

        fun showCityData(data: List<String>)

        fun showCityInfo(cityModel: CityForecastViewModel)
    }

    interface Presenter : BasePresentationContract.BasePresenter<View> {

        fun getForecastData()

        fun getNextDayData()

        fun getPreviousDayData()

        fun getCityData(city: String)
    }
}