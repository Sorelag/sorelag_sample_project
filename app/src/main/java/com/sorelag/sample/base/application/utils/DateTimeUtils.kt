package com.sorelag.sample.base.application.utils

import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtils {

    private const val INPUT_DATE_PATTERN = "yyyy-mm-dd"
    private const val OUTPUT_DATE_PATTERN = "MMMM d"

    /**
     *Formats date from "yyyy-mm-dd" to "dd MMMM" format
     */
    @JvmStatic
    fun formatDateToMonth(mDate: String): String {
        val inputFormat = SimpleDateFormat(INPUT_DATE_PATTERN, Locale.getDefault())
        val outputFormat = SimpleDateFormat(OUTPUT_DATE_PATTERN, Locale.getDefault())
        val date = inputFormat.parse(mDate)
        return outputFormat.format(date)
    }
}