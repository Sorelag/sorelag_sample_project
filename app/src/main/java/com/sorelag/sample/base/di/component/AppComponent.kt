package com.sorelag.sample.base.di.component

import com.sorelag.sample.base.application.MainApplication
import com.sorelag.sample.base.di.CoreComponent
import com.sorelag.sample.base.di.module.AppModule
import com.sorelag.sample.base.di.module.forecast.ForecastActivityComponent
import com.sorelag.sample.base.di.scopes.FeatureScope
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule

@FeatureScope
@Component(
    modules = [AppModule::class, AndroidInjectionModule::class, AndroidSupportInjectionModule::class],
    dependencies = [CoreComponent::class]
)
interface AppComponent {

    fun inject(app: MainApplication)

    fun forecastActivityComponent(): ForecastActivityComponent.Builder

    @Component.Builder
    interface Builder {

        fun appModule(appModule: AppModule): Builder

        fun coreComponent(coreComponent: CoreComponent): Builder

        fun build(): AppComponent
    }
}