package com.sorelag.sample.base.di.scopes

import javax.inject.Scope

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.TYPE, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.SOURCE)
@Scope
annotation class ActivityScope