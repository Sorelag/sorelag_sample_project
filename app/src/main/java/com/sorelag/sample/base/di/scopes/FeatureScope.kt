package com.sorelag.sample.base.di.scopes

import javax.inject.Scope

@Retention(AnnotationRetention.SOURCE)
@Scope
annotation class FeatureScope