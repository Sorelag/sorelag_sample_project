package com.sorelag.sample.base.application

import android.app.Activity
import android.app.Application
import android.app.Service
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDex
import com.facebook.stetho.Stetho
import com.sorelag.sample.BuildConfig
import com.sorelag.sample.base.di.CoreModule
import com.sorelag.sample.base.di.DaggerCoreComponent
import com.sorelag.sample.base.di.component.AppComponent
import com.sorelag.sample.base.di.component.DaggerAppComponent
import com.sorelag.sample.base.di.module.AppModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasServiceInjector
import timber.log.Timber
import javax.inject.Inject

class MainApplication : Application(), HasActivityInjector, HasServiceInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var serviceInjector: DispatchingAndroidInjector<Service>

    private var appComponent: AppComponent? = null

    fun getAppComponent(): AppComponent {
        checkNotNull(appComponent)
        return appComponent as AppComponent
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        injectDependency()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Stetho.initializeWithDefaults(this)
        }

        appComponent!!.inject(this)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    private fun injectDependency() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .coreComponent(
                DaggerCoreComponent.builder()
                    .coreModule(CoreModule(this))
                    .build()
            )
            .build()
    }

    override fun activityInjector(): DispatchingAndroidInjector<Activity>? {
        return activityInjector
    }

    override fun serviceInjector(): AndroidInjector<Service>? {
        return serviceInjector
    }
}
