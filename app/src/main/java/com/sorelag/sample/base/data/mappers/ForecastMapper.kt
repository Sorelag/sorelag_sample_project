package com.sorelag.sample.base.data.mappers

import com.sorelag.sample.base.data.entities.forecast.Forecast
import com.sorelag.sample.base.ui.viewModels.forecast.CityForecastViewModel
import com.sorelag.sample.base.ui.viewModels.forecast.ForecastDayViewModel
import com.sorelag.sample.base.ui.viewModels.forecast.ForecastViewModel

//TODO  Add mapper unit test
class ForecastMapper : BaseMapper<Forecast, ForecastViewModel>() {

    override fun transform(from: Forecast): ForecastViewModel? {
        val forecastViewModel = ForecastViewModel()
        val forecastDataList: MutableList<ForecastDayViewModel> = mutableListOf()

        for (forecast in from.forecasts) {
            val forecastDayViewModel = ForecastDayViewModel()
            forecastDayViewModel.date = forecast.date
            forecastDayViewModel.temperatureDayMin = forecast.day.tempmin
            forecastDayViewModel.temperatureDayMax = forecast.day.tempmax
            forecastDayViewModel.temperatureNightMin = forecast.night.tempmin
            forecastDayViewModel.temperatureNightMax = forecast.night.tempmax
            forecastDayViewModel.dayWeatherType = forecast.day.phenomenon
            forecastDayViewModel.nightWeatherType = forecast.night.phenomenon
            forecastDayViewModel.dayText = forecast.day.text
            forecastDayViewModel.nightText = forecast.night.text

            val forecastCityDataList: MutableList<CityForecastViewModel> = mutableListOf()

            val cityNameList: MutableList<String> = mutableListOf()
            cityNameList.add("Default")

            if (forecast.day.places != null) for (cityDayForecast in forecast.day.places) {

                val forecastCityViewModel = CityForecastViewModel()

                while (!cityNameList.contains(cityDayForecast.name)) {
                    cityNameList.add(cityDayForecast.name)
                }

                forecastCityViewModel.city = cityDayForecast.name
                forecastCityViewModel.temperatureDay = cityDayForecast.tempmax
                forecastCityViewModel.dayWeatherType = cityDayForecast.phenomenon

                if (forecast.night.places != null) {
                    for (cityNightForecast in forecast.night.places) if (cityDayForecast.name == cityNightForecast.name) {
                        forecastCityViewModel.temperatureNight = cityNightForecast.tempmin
                        forecastCityViewModel.nightWeatherType =
                            cityNightForecast.phenomenon
                        break
                    }
                }
                forecastCityDataList.add(forecastCityViewModel)
            }
            forecastDayViewModel.cityNames = cityNameList
            forecastDayViewModel.cityData = forecastCityDataList

            forecastDataList.add(forecastDayViewModel)
        }

        forecastViewModel.data = forecastDataList
        return forecastViewModel
    }
}