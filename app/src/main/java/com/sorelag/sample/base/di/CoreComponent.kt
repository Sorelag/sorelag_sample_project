package com.sorelag.sample.base.di

import android.content.Context
import com.sorelag.sample.base.application.network.NetworkApi
import com.sorelag.sample.base.application.utils.NetworkConnectionWrapper
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(CoreModule::class))
interface CoreComponent {

    fun provideContext(): Context

    fun provideApi(): NetworkApi

    fun networkConnectionWrapper(): NetworkConnectionWrapper

    @Component.Builder
    interface Builder {

        fun coreModule(coreModule: CoreModule): Builder

        fun build(): CoreComponent
    }
}