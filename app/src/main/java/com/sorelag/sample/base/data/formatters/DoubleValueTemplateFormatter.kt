package com.sorelag.sample.base.data.formatters

import android.text.Spannable
import android.text.SpannableString

class DoubleValueTemplateFormatter(
    private var template: String
) : BaseFormatter<Pair<Double, Double>, Spannable> {

    override fun format(from: Pair<Double, Double>): Spannable? {
        val formattedText = String.format(template, from.first.toInt(), from.second.toInt())
        return SpannableString(formattedText)
    }
}