package com.sorelag.sample.base.application.network

import com.sorelag.sample.base.data.entities.forecast.Forecast
import io.reactivex.Observable
import retrofit2.http.GET

interface NetworkApi {

    @GET("api/estonia/forecast")
    fun getData(): Observable<Forecast>
}