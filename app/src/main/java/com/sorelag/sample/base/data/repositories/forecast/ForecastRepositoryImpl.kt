package com.sorelag.sample.base.data.repositories.forecast

import com.sorelag.sample.base.application.network.NetworkApi
import com.sorelag.sample.base.data.entities.forecast.Forecast
import io.reactivex.Observable
import io.reactivex.Scheduler

class ForecastRepositoryImpl(private val api: NetworkApi, private val networkScheduler: Scheduler) :
    ForecastRepository {

    override fun getForecast(): Observable<Forecast> {
        return api.getData()
            .subscribeOn(networkScheduler)
    }
}