package com.sorelag.sample.base.data.entities.forecast

/**
{
    "phenomenon": "Light snow shower",
    "tempmin": 1.0,
    "tempmax": 5.0,
    "text": "Partly cloudy. Precipitation  at times. North, northwest wind 6-11, on coast in gusts 15 m/s. Air temperature 1..5°C, by evening falling inland below 0°C.",
    "sea": "",
    "peipsi": "Northwest wind 8-11, in gusts 14 m/s.Wave height 0,8-1,5 m. At times rain or sleet. Visibility good or moderate. Air temperature 1..3°C.",
    "places": [Places],
    "winds": [Winds]
}
 **/
data class ForecastParams(
    val phenomenon: String,
    val tempmin: Double?,
    val tempmax: Double?,
    val text: String,
    val sea: String?,
    val peipsi: String?,
    val places: List<Places>?,
    val winds: List<Winds>?
)