package com.sorelag.sample.base.ui.widgets

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.sorelag.sample.R

class ProgressView : FrameLayout {
    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.view_progress, this, true)
        if (attrs != null) {
            val typedArray =
                context.obtainStyledAttributes(attrs, intArrayOf(android.R.attr.background))
            background = typedArray.getDrawable(0)
            typedArray.recycle()
        }

        if (background != null) {
            background = background
        } else {
            setBackgroundColor(Color.WHITE)
        }
    }

    fun hide() {
        if (visibility == View.VISIBLE) {
            visibility = View.GONE
        }
    }

    fun show() {
        if (visibility == View.GONE) {
            visibility = View.VISIBLE
        }
    }
}