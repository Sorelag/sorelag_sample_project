package com.sorelag.sample.base.models.forecast

import com.sorelag.sample.base.data.entities.forecast.Forecast
import com.sorelag.sample.base.data.mappers.ForecastMapper
import com.sorelag.sample.base.data.repositories.forecast.ForecastRepository
import com.sorelag.sample.base.ui.viewModels.forecast.ForecastViewModel
import io.reactivex.Observable
import javax.inject.Inject

class ForecastModelImpl @Inject constructor(
    private val repository: ForecastRepository
) : ForecastModel {

    override fun getForecast(): Observable<ForecastViewModel> {
        return repository.getForecast()
            .map(this::convertDataToViewModel)
    }

    private fun convertDataToViewModel(forecast: Forecast): ForecastViewModel? {
        return ForecastMapper().transform(forecast)
    }
}