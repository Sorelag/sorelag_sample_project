package com.sorelag.sample.base.application.utils

import android.content.Context

class NetworkConnectionWrapper(val context: Context) {

    fun isNetworkAvailable(): Boolean {
        return ConnectionUtils().isNetworkAvailable(context)
    }
}