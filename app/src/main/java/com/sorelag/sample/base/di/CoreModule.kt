package com.sorelag.sample.base.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(
    includes = [RxModule::class,
        ApiModule::class]
)
class CoreModule(private val application: Application) {

    @Singleton
    @Provides
    fun provideApplication(): Application {
        return application
    }

    @Singleton
    @Provides
    internal fun provideContext(): Context {
        return application
    }
}