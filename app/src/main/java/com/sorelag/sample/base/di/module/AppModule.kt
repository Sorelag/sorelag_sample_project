package com.sorelag.sample.base.di.module

import android.app.Application
import com.sorelag.sample.base.di.RepositoryModule
import com.sorelag.sample.base.di.RxModule
import com.sorelag.sample.base.di.scopes.FeatureScope
import dagger.Module
import dagger.Provides

@Module(
    includes = [RepositoryModule::class,
        RxModule::class]
)
class AppModule(private val application: Application) {

    @FeatureScope
    @Provides
    fun provideApplication(): Application {
        return application
    }
}