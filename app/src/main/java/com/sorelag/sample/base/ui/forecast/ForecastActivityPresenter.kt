package com.sorelag.sample.base.ui.forecast

import android.annotation.SuppressLint
import com.sorelag.sample.base.application.utils.NetworkConnectionWrapper
import com.sorelag.sample.base.core.BasePresenter
import com.sorelag.sample.base.models.forecast.ForecastModel
import com.sorelag.sample.base.ui.viewModels.forecast.ForecastDayViewModel
import com.sorelag.sample.base.ui.viewModels.forecast.ForecastViewModel
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class ForecastActivityPresenter @Inject constructor(
    private val model: ForecastModel,
    private val connectionWrapper: NetworkConnectionWrapper,
    private val observerScheduler: Scheduler
) : BasePresenter<ForecastActivityContract.View>(),
    ForecastActivityContract.Presenter {

    private lateinit var viewModel: ForecastViewModel
    private var day: Int = 0

    companion object {
        private const val DEFAULT: String = "Default"
    }

    @SuppressLint("CheckResult")
    override fun getForecastData() {

        if (!connectionWrapper.isNetworkAvailable()) {
            view?.showNoConnection()
            return
        }

        model.getForecast().subscribeOn(Schedulers.io())
            .observeOn(observerScheduler)
            .subscribe({ forecastData ->
                viewModel = forecastData
                view?.showData(getDayData(day))
                view?.showCityData(getCityData(day))
            }, { _ ->
                view?.showError()
                view?.hideProgress()
            }, {
                view?.hideProgress()
            }, { _ -> view?.showProgress() })
    }

    // TODO find better solution
    override fun getNextDayData() {
        day++
        view?.showData(getDayData(day))
        view?.showCityData(getCityData(day))
        toggleButtonVisibility()
    }

    // TODO find better solution
    override fun getPreviousDayData() {
        day--
        view?.showData(getDayData(day))
        view?.showCityData(getCityData(day))
        toggleButtonVisibility()
    }

    override fun getCityData(city: String) {
        if (city != DEFAULT)
            for (cityModel in getDayData(day).cityData) {
                if (cityModel.city == city) {
                    view?.showCityInfo(cityModel)
                    break
                }
            } else {
            view?.showData(getDayData(day))
        }
    }

    private fun getDayData(day: Int): ForecastDayViewModel {
        return viewModel.data[day]
    }

    private fun getCityData(day: Int): List<String> {
        return viewModel.data[day].cityNames
    }

    private fun toggleButtonVisibility() {
        view?.toggleNextBtnVisibility(day < viewModel.data.size - 1)
        view?.togglePreviousBtnVisibility(day > 0)
    }
}
