package com.sorelag.sample.base.data.entities.forecast

import androidx.annotation.StringDef

/**
 * "Light snow shower"
 * "Light shower"
 * "Light sleet"
 * "Variable clouds"
 * "Light rain"
 * "Cloudy with clear spells"
 */
@Retention(AnnotationRetention.SOURCE)
@StringDef(
    WeatherType.LIGHT_SNOW_SHOWER,
    WeatherType.LIGHT_SHOWER,
    WeatherType.LIGHT_SLEET,
    WeatherType.VARIABLE_CLOUDS,
    WeatherType.LIGHT_RAIN,
    WeatherType.MODERATE_SLEET,
    WeatherType.MODERATE_RAIN
)
annotation class WeatherType {
    companion object {
        const val LIGHT_SNOW_SHOWER = "Light snow shower"
        const val LIGHT_SHOWER = "Light shower"
        const val LIGHT_SLEET = "Light sleet"
        const val VARIABLE_CLOUDS = "Variable clouds"
        const val LIGHT_RAIN = "Light rain"
        const val CLOUDY_CLEAR_SPELLS = "Cloudy with clear spells"
        const val MODERATE_SLEET = "Moderate sleet"
        const val MODERATE_RAIN = "Moderate rain"
    }
}