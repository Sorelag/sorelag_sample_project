package com.sorelag.sample.base.data.entities.forecast

/**
{
    "name": "Kuusiku",
    "direction": "Northwest wind",
    "speedmin": 6.0,
    "speedmax": 10.0
}
 **/
data class Winds(
    val name: String,
    val direction: String,
    val speedmin: Double,
    val speedmax: Double
)