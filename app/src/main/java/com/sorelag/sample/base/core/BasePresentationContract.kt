package com.sorelag.sample.base.core

import androidx.annotation.UiThread

interface BasePresentationContract {

    interface BaseView

    interface BasePresenter<V : BaseView> {

        @UiThread
        fun attachView(view: V)

        @UiThread
        fun detachView()
    }
}