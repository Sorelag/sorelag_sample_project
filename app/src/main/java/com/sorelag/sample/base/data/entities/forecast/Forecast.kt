package com.sorelag.sample.base.data.entities.forecast

/**
{
    "forecasts": List<[ForecastItem]>
}
 **/
data class Forecast(
    val forecasts: List<ForecastItem>
)