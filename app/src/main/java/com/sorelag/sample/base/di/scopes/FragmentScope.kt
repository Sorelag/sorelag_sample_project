package com.sorelag.sample.base.di.scopes

import javax.inject.Scope

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.TYPE)
@Retention(AnnotationRetention.SOURCE)
@Scope
annotation class FragmentScope